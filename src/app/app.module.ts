import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { LatbarComponent } from './components/latbar/latbar.component';
import { SetNameComponent } from './components/set-name/set-name.component';
import { SetTitleComponent } from './components/set-title/set-title.component';
import { SetSubtitleComponent } from './components/set-subtitle/set-subtitle.component';
import { RegistrosComponent } from './components/registros/registros.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LatbarComponent,
    SetNameComponent,
    SetTitleComponent,
    SetSubtitleComponent,
    RegistrosComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
