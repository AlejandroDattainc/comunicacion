import { Component } from '@angular/core';

import { User } from './models/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'Bienvenido al mundo de la comunicación';
  subtitle = 'Es un mundo asombroso';
  name = 'Alex';

  usuario: User = { nombre: 'Sin elegir', apellido: 'Sin elegir', edad: 0 };

  cambiarNombre(nombreNuevo: string) {
    this.name = nombreNuevo;
  }

  cambiarTitulo(tituloNuevo: string) {
    this.title = tituloNuevo;
  }

  cambiarSubtitulo(subtituloNuevo: string) {
    this.subtitle = subtituloNuevo;
  }

  recibirUsuario(user: User) {
    this.usuario = user;
    console.log(user);
  }

}
