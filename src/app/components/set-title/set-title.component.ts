import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-set-title',
  templateUrl: './set-title.component.html',
  styleUrls: ['./set-title.component.css']
})
export class SetTitleComponent implements OnInit {

  // evento para comunicar al padre
  @Output() public eventoNuevoTitulo = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  // funcion para emitir evento
  retornarNuevoTitulo(nuevoTitulo: string){
    this.eventoNuevoTitulo.emit(nuevoTitulo);
  }

}
