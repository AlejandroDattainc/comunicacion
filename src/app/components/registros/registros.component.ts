import { Component, OnInit, Output, EventEmitter } from '@angular/core';

// modelos 
import { User } from '../../models/user';

@Component({
  selector: 'app-registros',
  templateUrl: './registros.component.html',
  styleUrls: ['./registros.component.css']
})
export class RegistrosComponent implements OnInit {

  @Output() public eventoEnviarUsuario = new EventEmitter<User>()

  constructor() { }

  users: User[] = [
    { nombre: 'Alejandro', apellido: 'Aguilar', edad: 25 },
    { nombre: 'Esteban', apellido: 'Sin Apellido', edad: 35 },
    { nombre: 'David', apellido: 'Sin Apellido', edad: 35 },
  ]

  ngOnInit(): void {
  }

  enviarUsuario(user: User){
    this.eventoEnviarUsuario.emit(user);
    // console.log(user);
  }

}
