import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-set-name',
  templateUrl: './set-name.component.html',
  styleUrls: ['./set-name.component.css']
})
export class SetNameComponent implements OnInit {
  
  // parametro de entrada
  @Input() public nombre: string;
  // evento de salida de mensaje
  @Output() public eventoNuevoNombre = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  // funcion de emision de mensaje
  retornarNuevoNombre(nombreOutput: string){
    this.eventoNuevoNombre.emit(nombreOutput);
  }
}
