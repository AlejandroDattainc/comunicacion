import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetSubtitleComponent } from './set-subtitle.component';

describe('SetSubtitleComponent', () => {
  let component: SetSubtitleComponent;
  let fixture: ComponentFixture<SetSubtitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetSubtitleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetSubtitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
