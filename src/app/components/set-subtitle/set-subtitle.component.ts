import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-set-subtitle',
  templateUrl: './set-subtitle.component.html',
  styleUrls: ['./set-subtitle.component.css']
})
export class SetSubtitleComponent implements OnInit {

  @Output() public eventoNuevoSubtitulo = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  retornarNuevoSubtitulo(nuevoTitulo: string){
    this.eventoNuevoSubtitulo.emit(nuevoTitulo);
  }

}
