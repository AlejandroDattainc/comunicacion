import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LatbarComponent } from './latbar.component';

describe('LatbarComponent', () => {
  let component: LatbarComponent;
  let fixture: ComponentFixture<LatbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LatbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LatbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
