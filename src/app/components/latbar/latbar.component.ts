import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-latbar',
  templateUrl: './latbar.component.html',
  styleUrls: ['./latbar.component.css']
})
export class LatbarComponent implements OnInit {

  @Input() public nombreLatbar: string;
  
  constructor() { }

  ngOnInit(): void {
  }

}
