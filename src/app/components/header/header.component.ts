import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Input() public titulo: string;
  @Input() public subtitulo: string;

  @Input() public nombreHeader: string;

  constructor() { }

  ngOnInit(): void {
  }

}
